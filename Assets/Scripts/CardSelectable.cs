﻿using System.Collections;
using System.Collections.Generic;
//using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

public class CardSelectable : MonoBehaviour, IPointerClickHandler
{
    public float delayBeforeIdentifyingSameCards = 5.0f;
    public float delayBeforeBecomingChildOfValidationPoint = 2;
    public Card cardRef;
    public Transform cardValidationPointAssigned;
    public Transform cardDiscardPoint;
    public bool isThereSameCardTypeCurrentlyActive;
    public bool isSelected;
    public bool isCardSelectable;
    public bool canBeDiscarded;
    public bool didCheckSameCardType;
    //Once the turn is over then other card can be picked
    public bool isOtherCardTypeAlreadyPicked;
    public List<Card> allCardsList;
    public List<Card> similarCardTypeList;
    

    void OnEnable()
    {
        
        CardSelectionManager.Instance.OnCardSelectEvent += SelectCard;
    }

    void OnDisable()
    {
        CardSelectionManager.Instance.OnCardSelectEvent -= SelectCard;
    }

    IEnumerator Start()
    {
        if(!isCardSelectable)
        {
            this.GetComponent<Button>().interactable = false;
            isSelected = false;
        }

        if (this.gameObject.GetComponent<Card>() == null)
        {
            this.gameObject.AddComponent<Card>();
            cardRef = this.gameObject.GetComponent<Card>();
        }
        else
        {
            if (this.gameObject.GetComponent<Card>() != null)
                cardRef = this.gameObject.GetComponent<Card>();

            cardValidationPointAssigned = cardRef.validationPoint;

            cardDiscardPoint = cardRef.discardPoint;
        }

        

        yield return new WaitForSeconds(delayBeforeIdentifyingSameCards);

        if (!didCheckSameCardType)
        {
            allCardsList.AddRange(GameObject.FindObjectsOfType<Card>());
            CheckOtherSimilarCardType();

            didCheckSameCardType = true;
            isCardSelectable = true;
            this.GetComponent<Button>().interactable = true;

        }
    }

    void Update()
    {
        #region old stuff
        //if (isOtherCardAlreadyPicked)
        //{

        //    this.GetComponent<Button>().interactable = false;

        //}
        //else
        //{
        //    this.GetComponent<Button>().interactable = true;
        //}
        #endregion


        if(canBeDiscarded)
        {
            //MoveCard(cardDiscardPoint, cardRef.DiscardPointTypeTag, this.gameObject);
            //Debug.Log("Card should be discarded!");
            //Vector2.Lerp(this.transform.position, cardRef.discardPoint.position, Time.deltaTime * 2);

            this.gameObject.SetActive(false) ;
            this.GetComponent<Button>().interactable = false;

        }

        


    }

    /// <summary>
    /// Will check if there is an other card with the same type
    /// Will have a bool indicate that yes there is an other card with the same type
    /// Verify if other similar card has been selected
    /// if the other similar card has been selected then discard this one
    /// When the card is selected then the card that is not selected will be discarded
    /// </summary>
    public void CheckOtherSimilarCardType()
    {

        foreach (Card card in allCardsList)
        {
            if (cardRef.cardType == card.cardType)
            {
                isThereSameCardTypeCurrentlyActive = true;
                similarCardTypeList.Add(card);
            }
        }
    }

    public void MoveCard(Transform destPoint, string destPointTag, GameObject objToMove )
    {
        if (cardRef != null && this.GetComponent<MoveUIToPoint>() != null && isOtherCardTypeAlreadyPicked == false)
        {
            if (GameObject.FindGameObjectWithTag(cardRef.ValidationPointTypeTag) != null)//(GameObject.Find("CardClickedSpot") != null)
            {

                //cardValidationPointAssigned = GameObject.FindGameObjectWithTag(cardRef.ValidationPointTypeTag).transform;

                destPoint = GameObject.FindGameObjectWithTag(destPointTag).transform;

                //this.gameObject.GetComponent<MoveUIToPoint>().finalDestination = destPoint;

                //this.gameObject.

                this.GetComponent<Button>().interactable = false;

                objToMove.GetComponent<MoveUIToPoint>().MoveToPoint(objToMove.GetComponent<EasyTween>(), destPoint, this.gameObject.GetComponent<MoveUIToPoint>().easeCurveStart, this.gameObject.GetComponent<MoveUIToPoint>().easeCurveStart);


            }
        }
    }

    IEnumerator BecomeAChildInAFewSeconds(GameObject childObj, Transform parentObj, float delay)
    {
        yield return new WaitForSeconds(delay);
        childObj.transform.SetParent(childObj.GetComponent<Card>().validationPoint);

    }

    public void RunBecomeAChildInAFewSeconds(GameObject childObj, Transform parentObj, float delay)
    {
        StartCoroutine(BecomeAChildInAFewSeconds(childObj,  parentObj,  delay));
    }

    public void SelectCard()
    {
        //Output to console the clicked GameObject's name and the following message. You can replace this with your own actions for when clicking the GameObject.
        Debug.Log(gameObject.name + " Has Been Clicked!");
        isSelected = true;

        //CaloriesManager.Instance.totalCaloriesOfCurrentMeal = 
        
        CaloriesManager.Instance.totalCaloriesOfCurrentMeal += this.gameObject.GetComponent<Card>().calories;
        //if(Mathf.Sign(CaloriesManager.Instance.totalCaloriesOfCurrentMeal) >= 1)
        Player.Instance.currentCalories = CaloriesManager.Instance.CalculateCalories(Player.Instance.currentCalories, this.gameObject.GetComponent<Card>().calories);

        foreach (Card card in similarCardTypeList)
        {

            if(!card.GetComponent<CardSelectable>().isSelected)
            {
                card.GetComponent<CardSelectable>().isOtherCardTypeAlreadyPicked = true;
                card.GetComponent<CardSelectable>().canBeDiscarded = true;
            }
        }

        MoveCard(cardValidationPointAssigned, cardRef.ValidationPointTypeTag, this.gameObject);

        CardSelectionManager.Instance.currentCardMealSelected.Add(this.gameObject);

        if (CardSelectionManager.Instance.currentCardMealSelected.Count >= 4)
        {
            CardSelectionManager.Instance.shouldCalculateCardsSelected = true;

            foreach(GameObject card in CardSelectionManager.Instance.currentCardMealSelected)
            {
                //card.transform.SetParent(card.GetComponent<Card>().validationPoint);
                RunBecomeAChildInAFewSeconds(card, card.GetComponent<Card>().validationPoint, delayBeforeBecomingChildOfValidationPoint);
            }
        }
    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        if(!isSelected)
        SelectCard();
    }

    #region 
    //[SerializeField]
    //protected Button m_button;
    //[SerializeField]
    //protected Image m_portrait;
    //[SerializeField]
    //protected GameObject m_selectedOverlay;

    //public event Action<UICharacterSelectable> selectAction = delegate { };
    //public event Action<UICharacterSelectable> deselectAction = delegate { };

    //public EntityData data
    //{
    //    get { return m_data; }
    //}

    //public void setData(EntityData p_data)
    //{
    //    m_data = p_data;
    //    m_button.onClick.AddListener(select);
    //    m_selectedOverlay.SetActive(false);
    //    m_portrait.sprite = m_data.portrait;
    //}

    //public virtual void select()
    //{
    //    m_button.onClick.RemoveListener(select);
    //    m_button.onClick.AddListener(deselect);

    //    m_selectedOverlay.SetActive(true);

    //    selectAction(this);
    //}

    //public virtual void deselect()
    //{
    //    m_button.onClick.AddListener(select);
    //    m_button.onClick.RemoveListener(deselect);

    //    m_selectedOverlay.SetActive(false);

    //    deselectAction(this);
    //}

    //protected EntityData m_data;
    #endregion
}
