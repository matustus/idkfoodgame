﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Moonflows.ClassHelpers;

public class ValidationArea : Singleton<ValidationArea>
{
	public List<GameObject> allValidationPoints;

	public bool allFull = false;

	public int validationPointsCounter =0;


	void Awake()
	{
		
	}

	// Use this for initialization
	void Start ()
	{
		//StartCoroutine(CheckPoints());
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(MainGameController.Instance.Gamestates == MainGameController.GAMESTATES.PLAYING)
		{
			CheckSlotsFull();
		}
	}

	void CheckSlotsFull()
	{
		if(validationPointsCounter < allValidationPoints.Count)
		{
			allFull = false;
			return;
		}
		else if(validationPointsCounter == allValidationPoints.Count)
		{
			allFull = true;
			validationPointsCounter = allValidationPoints.Count;
			ResetValues();
		}
	}

	void ResetValues()
	{
		allFull = false;
		validationPointsCounter = 0;
	}

	IEnumerator SetupPoints()
	{
		//yield return new WaitUntil(() => MainGameController.Instance.Gamestates == MainGameController.GAMESTATES.PLAYING );
		yield return null;
		//StartCoroutine(CheckPoints());
	}
}
