﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Moonflows.ClassHelpers;

public class Player : Singleton<Player>
{
    public int currentCalories;
    public bool isPlayerOverWeight;
    public bool isPlayerWeightGood;
    public bool isPlayerUnderWeight;
    public bool isGameOver;

    void Init()
    {
        currentCalories = CaloriesManager.Instance.initialPlayerCalories;
    }



	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        PlayerWeightWatcher();

    }

    public void PlayerWeightWatcher()
    {
        if (currentCalories <= CaloriesManager.Instance.playerMinCaloriesPossible)
        {
            currentCalories = CaloriesManager.Instance.playerMinCaloriesPossible;
            isPlayerUnderWeight = true;
            isPlayerWeightGood = false;
            isGameOver = true;
            MainGameController.Instance.Gamestates = MainGameController.GAMESTATES.GAMEOVER;
        }
        else if (currentCalories >= CaloriesManager.Instance.playerMaxCaloriesPossible)
        {
            currentCalories = CaloriesManager.Instance.playerMaxCaloriesPossible;
            isPlayerOverWeight = true;
            isPlayerWeightGood = false;
            MainGameController.Instance.Gamestates = MainGameController.GAMESTATES.GAMEOVER;
        }
        else
        {
            isPlayerWeightGood = true;
            isPlayerUnderWeight = false;
            isPlayerOverWeight = false;
            isGameOver = true;
            
        }
    }

    public void changePlayerSideBasedOnCalories()
    {

    }
}
