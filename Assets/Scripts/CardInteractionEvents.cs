﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UITween;

public class CardInteractionEvents : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler
{
    public float moveSpeed;
	public Transform validationPointTransform;

	private Card checkCardType;

    public void Init()
    {
        //Here we should check the card type
        //Based on the card type a validation point should be assign for that card type
		if (this.checkCardType.cardType.ToString() == "MAINDISH")
		{
			if (GameObject.FindGameObjectWithTag("ValidationPointMainDish") != null)
				this.validationPointTransform = GameObject.FindGameObjectWithTag("ValidationPointMainDish").transform;
		}
		else if (this.checkCardType.cardType.ToString() == "SIDEDISH")
		{
			if (GameObject.FindGameObjectWithTag("ValidationPointSideDish") != null)
				this.validationPointTransform = GameObject.FindGameObjectWithTag("ValidationPointSideDish").transform;

		}

		else if (this.checkCardType.cardType.ToString() == "DESSERT")
		{
			if (GameObject.FindGameObjectWithTag("ValidationPointDessert") != null)
				this.validationPointTransform = GameObject.FindGameObjectWithTag("ValidationPointDessert").transform;
		}

		if (this.checkCardType.cardType.ToString() == "DRINK")
		{
			if (GameObject.FindGameObjectWithTag("ValidationPointDrink") != null)
				this.validationPointTransform = GameObject.FindGameObjectWithTag("ValidationPointDrink").transform;
		}
    }


    // Use this for initialization
    void Start()
    {
		this.checkCardType = this.GetComponent<Card>();

		Init();
    }

	void Update()
	{
		Debug.Log("Card Counter" + ReceiptManager.Instance.cardCounter);
	}

    /// <summary>
    /// 
    /// </summary>
    public void MoveToValidationPos()
    {
		//this.gameObject.transform.SetParent(this.validationPointTransform);
		this.gameObject.transform.position = this.validationPointTransform.position;
    }

    /// <summary>
    /// 
    /// </summary>
    public void SlightScaleUp()
    {

    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        MoveToValidationPos();

		ValidationArea.Instance.validationPointsCounter ++;

		//if (MainGameController.Instance.EatingPeriod.Equals(MainGameController.EATINGPERIOD.BREAKFAST))
		//	Debug.Log("breakfast" );
		// TODO use a switch case to make this clear
		switch( MainGameController.Instance.EatingPeriod )
		{
			case MainGameController.EATINGPERIOD.BREAKFAST:
			{
				GameObject cardClone = Instantiate(this.gameObject);	
				cardClone.transform.SetPositionAndRotation(ReceiptManager.Instance.breakfastSlots[ReceiptManager.Instance.cardCounter].transform.position,ReceiptManager.Instance.breakfastSlots[ReceiptManager.Instance.cardCounter].transform.rotation); // = ;
				cardClone.transform.SetParent(ReceiptManager.Instance.breakfastSlots[ReceiptManager.Instance.cardCounter].transform,false);

				cardClone.GetComponent<Card>().enabled = false;
				cardClone.GetComponent<MoveUIToPoint>().enabled = false;
				cardClone.GetComponent<EasyTween>().enabled = false;
				cardClone.GetComponent<CardSelectable>().enabled = false;

				ReceiptManager.Instance.cardCounter ++;
				if(ReceiptManager.Instance.cardCounter >= ReceiptManager.Instance.breakfastSlots.Count)
				{
					ReceiptManager.Instance.cardCounter = 0;
					MainGameController.Instance.EatingPeriod = MainGameController.EATINGPERIOD.LUNCH;

					CardSelectionManager.Instance.ClearCards();

					DealPlayerCards.Instance.RunSpawnCards();

					return;
				}
				break;
			}
			case MainGameController.EATINGPERIOD.LUNCH:
			{
				GameObject cardClone = Instantiate(this.gameObject);	
				cardClone.transform.SetPositionAndRotation(ReceiptManager.Instance.lunchSlots[ReceiptManager.Instance.cardCounter].transform.position,ReceiptManager.Instance.lunchSlots[ReceiptManager.Instance.cardCounter].transform.rotation); // = ;
				cardClone.transform.SetParent(ReceiptManager.Instance.lunchSlots[ReceiptManager.Instance.cardCounter].transform,false);

				cardClone.GetComponent<Card>().enabled = false;
				cardClone.GetComponent<MoveUIToPoint>().enabled = false;
				cardClone.GetComponent<EasyTween>().enabled = false;
				cardClone.GetComponent<CardSelectable>().enabled = false;

				ReceiptManager.Instance.cardCounter ++;
				if(ReceiptManager.Instance.cardCounter >= ReceiptManager.Instance.lunchSlots.Count)
				{
					ReceiptManager.Instance.cardCounter = 0;
					MainGameController.Instance.EatingPeriod = MainGameController.EATINGPERIOD.DINNER;

					CardSelectionManager.Instance.ClearCards();

					DealPlayerCards.Instance.RunSpawnCards();

					return;
				}
				break;
			}
			case MainGameController.EATINGPERIOD.DINNER:
			{
				GameObject cardClone = Instantiate(this.gameObject);	
				cardClone.transform.SetPositionAndRotation(ReceiptManager.Instance.dinnerSlots[ReceiptManager.Instance.cardCounter].transform.position,ReceiptManager.Instance.dinnerSlots[ReceiptManager.Instance.cardCounter].transform.rotation); // = ;
				cardClone.transform.SetParent(ReceiptManager.Instance.dinnerSlots[ReceiptManager.Instance.cardCounter].transform,false);

				cardClone.GetComponent<Card>().enabled = false;
				cardClone.GetComponent<MoveUIToPoint>().enabled = false;
				cardClone.GetComponent<EasyTween>().enabled = false;
				cardClone.GetComponent<CardSelectable>().enabled = false;

				ReceiptManager.Instance.cardCounter ++;
				if(ReceiptManager.Instance.cardCounter >= ReceiptManager.Instance.dinnerSlots.Count)
				{
					ReceiptManager.Instance.cardCounter = 0;
					MainGameController.Instance.EatingPeriod = MainGameController.EATINGPERIOD.NONE;
					MainGameController.Instance.Gamestates = MainGameController.GAMESTATES.INTERMISSION;

					ReceiptManager.Instance.ShowTimedReceipt();
					return;
				}
				break;
			}
			case MainGameController.EATINGPERIOD.NONE:
			{
				break;
			}
			default:
				break;
		}

		this.GetComponent<Image>().raycastTarget = false;
    }
		
    public void OnPointerEnter(PointerEventData pointerEventData)
    {

    }

}

