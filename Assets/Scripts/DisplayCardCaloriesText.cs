﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayCardCaloriesText : MonoBehaviour
{

    public Text caloriesText;
    public Text cardNameText;

	// Use this for initialization
	void Start ()
    {
        if(caloriesText != null && this.GetComponent<Card>() != null)
        caloriesText.text = this.GetComponent<Card>().calories.ToString();

        //if (cardNameText.text == null)
            this.transform.Find("CardNameText").GetComponent<Text>().text = this.gameObject.GetComponent<Card>().cardName;
            //cardNameText.text = this.gameObject.name;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
