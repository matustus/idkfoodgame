﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValidationPoint : MonoBehaviour
{
	public bool isOccupied;

	void OnEnable()
	{
		this.isOccupied = false;
	}

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public Transform CheckOccupied()
	{
		//if(this.transform.childCount > 0)
		if(this.transform.GetComponentInChildren<Card>())
		{
			this.isOccupied = true;
			return this.transform.GetChild(0);
		}
		else
		{
			this.isOccupied = false;
			return null;
		}
	}
}
