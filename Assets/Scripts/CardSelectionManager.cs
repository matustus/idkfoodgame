﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
//using DG.Tweening;
using UnityEngine.EventSystems;
using Moonflows.ClassHelpers;

public class CardSelectionManager : Singleton<CardSelectionManager>
{
    public float startDelay = 2.25f;
    //public CardSelectable cardSelected;
    public List<GameObject> currentCardSelectableList;
    public List<GameObject> currentCardMealSelected;
    public bool shouldCalculateCardsSelected;
    //public bool isNewCardIn = false;
    //public bool shouldMoveSelectedCardToEndPoint;
    //public bool shouldRemakeCardInteractable;
    //public bool isCardSelected;

    //public GameObject cardSelectedObj;
    public GameObject validationMealPanel;

    public delegate void SelectCardHandler();
    public event SelectCardHandler OnCardSelectEvent;

    private void Init()
    {
        if (validationMealPanel != null)
            validationMealPanel.transform.gameObject.SetActive(false);
    }

    IEnumerator Start()
    {
        //Init();
        yield return new WaitForSeconds(startDelay);
        currentCardSelectableList.AddRange(GameObject.FindGameObjectsWithTag("playerCard"));
    }

    private void Update()
    {
        CheckIfCardInListSelected();
    }

    public void CheckIfCardInListSelected()
    {
        if (currentCardSelectableList.Count > 0)
        {
            foreach (GameObject card in currentCardSelectableList)
            {
                if (card.GetComponent<CardSelectable>() != null)//if(card.isSelected && card.isOtherCardAlreadyPicked == false)
                {
                    if (card.GetComponent<CardSelectable>().isSelected && card.GetComponent<CardSelectable>().isCardSelectable)
                    {

                        if (validationMealPanel != null && !validationMealPanel.gameObject.activeSelf)
                            validationMealPanel.transform.gameObject.SetActive(true);


                    }

                }
            }
        }
    }

	public void ClearCards()
	{
		currentCardSelectableList.Clear();
	}
}
