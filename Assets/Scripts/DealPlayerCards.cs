﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Moonflows.ClassHelpers;

public class DealPlayerCards : Singleton<DealPlayerCards>
{
    public bool allMealCardsSpawned;
    public bool canDealNextMeal;
    public float delayBewteenSpawn = 1.0f;
    public float delayBeforeBecomingChild = 1.0f;
    public Transform deckCanvas;
    public Transform cardSelectionGroup;
    public Transform spawnPoint;
    public Transform[] initPoints;
    //public int numberOfDecks;
    //public int numbOfCardPerDeck;
    public int cardNumberSpawn;
    //public GameObject playerCard;
    public GameObject cardClone;
    

    void Init()
    {
        //RunSpawnCards();
    }

	// Use this for initialization
	void Start ()
    {
        //Init();

    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void RunSpawnCards()
    {
        StartCoroutine(SpawnCards());
    }

    IEnumerator SpawnCards()
    {
        #region currently working fine
        //if (playerCard != null && spawnPoint != null && deckCanvas != null)
        //{
        //    for (int x = 0; x < cardNumberSpawn; x++)
        //    {
        //        yield return new WaitForSeconds(delayBewteenSpawn);

        //        cardClone = Instantiate(playerCard, spawnPoint);
        //        cardClone.transform.SetParent(deckCanvas);
        //        //cardClone.transform.parent = deckCanvas;
        //        SetUpCardsInitialDestination(cardClone, initPoints[x]);

        //        //StartCoroutine(SetUpCardsDestination(cardClone, stopPoints[x], cardSelectionGroup));
        //    }
        //}
        #endregion

     

        if ( spawnPoint != null && deckCanvas != null)
        {
           
                for(int x = 0; x < cardNumberSpawn; x++)
                {
                    yield return new WaitForSeconds(delayBewteenSpawn);

                if (x < 2)
                {
                    cardClone = Instantiate(PickRandomCard(Deck.Instance.mainDishDeck), spawnPoint);

                }
                else if (x >= 2 && x < 4)
                {
                    cardClone = Instantiate(PickRandomCard(Deck.Instance.sideDishDeck), spawnPoint);
                }
                else if (x >= 4 && x < 6)
                {
                    cardClone = Instantiate(PickRandomCard(Deck.Instance.dessertDeck), spawnPoint);
                }
                else if (x >= 6 && x < 8)
                {
                    cardClone = Instantiate(PickRandomCard(Deck.Instance.drinkDeck), spawnPoint);
                }

                if(x <= cardNumberSpawn)
                {
                    allMealCardsSpawned = true;
                }
             
                //cardClone = Instantiate(PickRandomCard(Deck.Instance.sideDishDeck), spawnPoint);
                cardClone.transform.SetParent(deckCanvas);
                SetUpCardsInitialDestination(cardClone, initPoints[x]);
            }

            //}
        }
    }

    public GameObject PickRandomCard(List<GameObject> cardObjList)
    {
        GameObject randObjPicked = null;

        randObjPicked = cardObjList[Random.Range(0, cardObjList.Count)];  //Deck.Instance.mainDishDeck[Random];

        return randObjPicked;
    }

    /// <summary>
    /// Make sure the cards have a stop point to go to after being spawn
    /// Also change parent for better readibility
    /// Make sure you are using EasyTween UI before using this
    /// </summary>
    /// <param name="movingCard"></param>
    /// <param name="endPos"></param>
    void SetUpCardsInitialDestination(GameObject movingCard, Transform endPos)
    {
        if (movingCard.GetComponent<MoveUIToPoint>() != null && endPos != null)
        {
            movingCard.GetComponent<MoveUIToPoint>().initialDestination = endPos;
            //movingCard.transform.parent = endPos;
        }
    }

    void SetUpCardsFinalDestination(GameObject movingCard, Transform endPos)
    {
        if (movingCard.GetComponent<MoveUIToPoint>() != null && endPos != null)
        {
            movingCard.GetComponent<MoveUIToPoint>().finalDestination = endPos;
            //movingCard.transform.parent = endPos;
        }
    }

    //IEnumerator SetUpCardsDestination(GameObject movingCard, Transform endPos, Transform futureParent)
    //{
    //    if (movingCard.GetComponent<MoveUIToPoint>() != null && endPos != null && futureParent != null)
    //    {
    //        movingCard.GetComponent<MoveUIToPoint>().destination = endPos;

    //        yield return new WaitForSeconds(delayBeforeBecomingChild);
    //        movingCard.transform.parent = futureParent;
    //    }
    //}


}
