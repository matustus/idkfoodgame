﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveUIToPoint : MonoBehaviour {

    public bool shouldMoveToAPoint;
    public bool isObjMoveOnInit;
    public float startDelay = 1.2f;
    public Transform initialDestination;
    public Transform finalDestination;
    public AnimationCurve easeCurveStart;
    public AnimationCurve easeCurveEnd;
    public EasyTween TweenToControl;

    private void Init()
    {
        if(this.GetComponent<Card> () != null)
        {
            finalDestination = this.GetComponent<Card>().validationPoint;
        }

        if (isObjMoveOnInit)
        {
            MoveToPoint(TweenToControl, initialDestination, easeCurveStart, easeCurveEnd);
        }
    }

    // Use this for initialization
    IEnumerator Start ()
    {
        yield return new WaitForSeconds(startDelay);
        Init();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //if (shouldMoveToAPoint)
        //{
        //    MoveToPoint(TweenToControl, finalDestination, easeCurveStart, easeCurveEnd);
        //}
    }

    public void MoveToPoint(EasyTween uiObjToTween, Transform dest, AnimationCurve animationCurveStart, AnimationCurve animationCurveEnd)
    {
        //TweenToControl = this.GetComponent<EasyTween>();

        if (uiObjToTween != null && dest != null)
        {
            uiObjToTween.SetAnimationPosition(TweenToControl.rectTransform.anchoredPosition, (Vector2)dest.localPosition, animationCurveStart, animationCurveEnd);

            uiObjToTween.OpenCloseObjectAnimation();
        }
    }

    public void MoveToPoint( Transform dest, AnimationCurve animationCurveStart, AnimationCurve animationCurveEnd)
    {
        //TweenToControl = this.GetComponent<EasyTween>();

        if (TweenToControl != null && dest != null)
        {
            TweenToControl.SetAnimationPosition(TweenToControl.rectTransform.anchoredPosition, (Vector2)dest.localPosition, animationCurveStart, animationCurveEnd);

            TweenToControl.OpenCloseObjectAnimation();
        }
    }


}
