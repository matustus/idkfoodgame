﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Moonflows.ClassHelpers;
using UnityEngine.UI;

public class CaloriesManager : Singleton<CaloriesManager>
{
    [Header("Obj Cals & Text")]
    public Text currentPlayerCaloriesText;
    public string maxCaloriesText;

    [Space(5)]
    [Header("Cals variable")]
    public int totalCaloriesOfCurrentMeal;
    public int initialPlayerCalories;
    //public int playerCurrentCaloriesIntake;
    public int playerMaxCaloriesPossible;
    public int playerMinCaloriesPossible;

    
   

    void Init()
    {
        Player.Instance.currentCalories = initialPlayerCalories;
        maxCaloriesText = "/" + playerMaxCaloriesPossible.ToString();
    }

    private void OnEnable()
    {
        Init();
    }

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        UpdateCurrentCaloriesDisplay();


        if (CardSelectionManager.Instance.shouldCalculateCardsSelected)
        {
            
        }
	}

    public void UpdateCurrentCaloriesDisplay()
    {
        if(currentPlayerCaloriesText != null)
        currentPlayerCaloriesText.text = Player.Instance.currentCalories.ToString() + maxCaloriesText;
    }

    public int CalculateCalories(int a, int b)
    {
        int total;

        total = a + b;
       

        return total;
    }
}
