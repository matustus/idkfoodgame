﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Moonflows.ClassHelpers;

public class ReceiptManager : Singleton<ReceiptManager>
{
	public GameObject receiptObject;

	public List<ValidationPoint> breakfastSlots;
	public List<ValidationPoint> lunchSlots;
	public List<ValidationPoint> dinnerSlots;

	private MoveUIToPoint moveuiPoint;

	public int cardCounter;

	public Transform OutScreen;
	public Transform OnScreen;

	void Awake()
	{
		moveuiPoint = this.GetComponent<MoveUIToPoint>();
	}

	// Use this for initialization
	void Start ()
	{
		StartCoroutine(SetupSlots());	
	}

	// Update is called once per frame
	void Update ()
	{
		
	}

	IEnumerator SetupSlots()
	{
		yield return new WaitUntil(() => MainGameController.Instance.Gamestates == MainGameController.GAMESTATES.PLAYING);
	}

	public void ShowReceipt()
	{
		this.gameObject.transform.SetPositionAndRotation(OnScreen.position,Quaternion.identity);
	}

	public void ShowTimedReceipt()
	{
		StartCoroutine(TimedReceipt());
	}

	private IEnumerator TimedReceipt()
	{
		this.gameObject.transform.SetPositionAndRotation(OnScreen.position,Quaternion.identity);

		yield return new WaitForSeconds(5.0f);

		this.gameObject.transform.SetPositionAndRotation(OutScreen.position,Quaternion.identity);

		MainGameController.Instance.nextMealText.SetActive(true);

		yield return new WaitForSeconds(5.0f);

		MainGameController.Instance.nextMealText.SetActive(false);

		MainGameController.Instance.NextDay();
	}

	// TODO: Call this function directly from whatever is on the screen
	public void HideReceipt()
	{
		this.gameObject.transform.SetPositionAndRotation(OutScreen.position,Quaternion.identity);

		if(MainGameController.Instance.Gamestates == MainGameController.GAMESTATES.INTERMISSION)
		{
			MainGameController.Instance.NextDay();

			for(int i =0; i < breakfastSlots.Count; i++)
			{
				Destroy(breakfastSlots[i].gameObject);
			}

			for(int j =0; j < lunchSlots.Count; j++)
			{
				Destroy(lunchSlots[j].gameObject);
			}

			for(int k = 0; k < dinnerSlots.Count; k++)
			{
				Destroy(dinnerSlots[k].gameObject);
			}
		}
		else if(MainGameController.Instance.Gamestates == MainGameController.GAMESTATES.PLAYING)
		{
			return;
		}
	}
}



































