﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour
{
    public enum CardType { MAINDISH, SIDEDISH, DESSERT, DRINK };
    public CardType cardType;

    public string cardName;
    [Range(-1000, 1000)]
	public int calories;
    [Range(-1000, 1000)]
    public int minRandCalories;
    [Range(-1000, 1000)]
    public int maxRandCalories;
	public Transform validationPoint;
    public Transform discardPoint;
    public bool canPickRandomCaloriesAmount;
    public bool shouldMoveToAPoint;
    public string ValidationPointTypeTag;
    public string DiscardPointTypeTag;

    public void Init()
    {
        

        if(GetComponent<EasyTween>() == null)
        this.gameObject.AddComponent<EasyTween>();

        if (this.cardType == CardType.MAINDISH)
        {
            if (GameObject.FindGameObjectWithTag("ValidationPointMainDish") != null)
                validationPoint = GameObject.FindGameObjectWithTag("ValidationPointMainDish").transform;

            if (GameObject.FindGameObjectWithTag("DiscardPointMainDish") != null)
                discardPoint = GameObject.FindGameObjectWithTag("DiscardPointMainDish").transform;

            ValidationPointTypeTag = "ValidationPointMainDish";
            DiscardPointTypeTag = "DiscardPointMainDish";

        }
        else if (this.cardType == CardType.SIDEDISH)
        {
            if (GameObject.FindGameObjectWithTag("ValidationPointSideDish") != null)
                validationPoint = GameObject.FindGameObjectWithTag("ValidationPointSideDish").transform;

            if (GameObject.FindGameObjectWithTag("DiscardPointSideDish") != null)
                discardPoint = GameObject.FindGameObjectWithTag("DiscardPointSideDish").transform;

            ValidationPointTypeTag = "ValidationPointSideDish";
            DiscardPointTypeTag = "DiscardPointSideDish";

        }

        else if (this.cardType == CardType.DESSERT)
        {
            if (GameObject.FindGameObjectWithTag("ValidationPointDessert") != null)
                validationPoint = GameObject.FindGameObjectWithTag("ValidationPointDessert").transform;

            if (GameObject.FindGameObjectWithTag("DiscardPointDessert") != null)
                discardPoint = GameObject.FindGameObjectWithTag("DiscardPointDessert").transform;

            ValidationPointTypeTag = "ValidationPointDessert";
            DiscardPointTypeTag = "DiscardPointDessert";
        }

        if (this.cardType == CardType.DRINK)
        {
            if (GameObject.FindGameObjectWithTag("ValidationPointDrink") != null)
                validationPoint = GameObject.FindGameObjectWithTag("ValidationPointDrink").transform;

            if (GameObject.FindGameObjectWithTag("DiscardPointDrink") != null)
                discardPoint = GameObject.FindGameObjectWithTag("DiscardPointDrink").transform;

            ValidationPointTypeTag = "ValidationPointDrink";
            DiscardPointTypeTag = "DiscardPointDrink";

        }

        Debug.Log("This card type is: " + cardType.ToString());

        //shouldMoveToAPoint = true;

        if(canPickRandomCaloriesAmount)
        {
            AssignRandCalories();
        }
    }

    void OnEnable()
    {

    }

    void Start()
    {
        Init();
    }

   public void AssignRandCalories()
    {
        calories = Random.Range(minRandCalories, maxRandCalories);
    }
}
