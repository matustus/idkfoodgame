﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Moonflows.ClassHelpers;

public class MainGameController : Singleton<MainGameController>
{
    public enum GAMESTATES { NONE, EARLYSTART, SETUP, GAMESTART, PLAYING, GAMEOVER, INTERMISSION }
    public enum EATINGPERIOD { NONE, BREAKFAST, LUNCH, DINNER }
    public enum GAMEPLAYSTATES { NONE, NOTENOUGHCALORIES, GOOD, TOOMUCHCALORIES}

    public GAMESTATES Gamestates;
    public EATINGPERIOD EatingPeriod;
    public GAMEPLAYSTATES GameplayStates;
    

    public float startDelay = 1;

	public GameObject introObject;

	public bool nextDay;

	public GameObject nextMealText;

    public void Awake()
    {
        if(introObject != null)
			introObject.SetActive(false);

		EatingPeriod = EATINGPERIOD.NONE;
		Gamestates = GAMESTATES.EARLYSTART;
		GameplayStates = GAMEPLAYSTATES.NONE;
    }


	// Use this for initialization
	IEnumerator Start ()
    {
		ShowLogo();

		yield return new WaitForSeconds(startDelay);

        SetUpGameElements();   
	}

	
	// Update is called once per frame
	void Update ()
    {
		Debug.Log("Current Eating Period"+ EatingPeriod);
		Debug.Log("Current Game State"+Gamestates);
		Debug.Log("Current Game Play"+GameplayStates);
	}

    /// <summary>
    /// 
    /// </summary>
    public void EatingPeriodSequence()
    {

    }

    /// <summary>
    /// 
    /// </summary>
    public void ShowLogo()
    {

    }

    /// <summary>
    /// Calories is kinda like your score/health
    /// </summary>
    public void ShowCalories()
    {

    }

    /// <summary>
    /// This function is going to set up art related stuff at the start of the game
    /// </summary>
    public void SetUpGameElements()
    {
		if(introObject != null)
			introObject.SetActive(true);

		Gamestates = GAMESTATES.SETUP;
	}

	public void FinishIntro()
	{
		introObject.SetActive(false);
		Gamestates = GAMESTATES.GAMESTART;

		StartPlaying();
	}

	public void GameStart()
	{
		Gamestates = GAMESTATES.GAMESTART;


	}

	public void StartPlaying()
	{
		DealPlayerCards.Instance.RunSpawnCards();
		// Setup the beginning of the game, everything must be ok
		Gamestates = GAMESTATES.PLAYING;

		GameplayStates = GAMEPLAYSTATES.GOOD;

		EatingPeriod = EATINGPERIOD.BREAKFAST;

		PickCardType();

		EatingPeriodSequence();

		ShowCalories();
	}

    /// <summary>
    /// Randomly assign 3 different card to a deck of type
    /// </summary>
    public void PickCardType()
    {

    }

	public void NextDay()
	{
		EatingPeriod = EATINGPERIOD.BREAKFAST;
		Gamestates = GAMESTATES.PLAYING;


	}

	/// <summary>
	/// You know it!!
	/// </summary>
	public void GameOver()
	{
		EatingPeriod = EATINGPERIOD.NONE;
		Gamestates = GAMESTATES.GAMEOVER;
	}

}
