﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Moonflows.ClassHelpers
{
	/// <summary>
	/// Singleton pattern.
	/// </summary>
	public class Singleton<T> : MonoBehaviour	where T : Component
	{
		/// <summary>
		/// The _instance.
		/// Small review on static
		//Static members variable belong to the class while a non static belong to the instance of the class
		//Changing a static in one place variable will change it everywhere
		/// </summary>
		protected static T _instance;
		
		/// <summary>
		/// Singleton design pattern
		/// </summary>
		/// <value>The instance.</value>
		public static T Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = FindObjectOfType<T> ();
					if (_instance == null)
					{
						GameObject obj = new GameObject ();
						//obj.hideFlags = HideFlags.HideAndDontSave;
						_instance = obj.AddComponent<T> ();
					}
				}
				return _instance;
			}
		}
		
		/// <summary>
		/// On awake, we initialize our instance. Make sure to call base.Awake() in override if you need awake.
		/// </summary>
		protected virtual void Awake ()
		{
			_instance = this as T;			
		}
	}


	/// <summary>
	/// Persistant Singleton design pattern
	/// </summary>
	public class PersistantSingleton <T>  : MonoBehaviour where T : Component
	{
		protected static T _instance;
		
		/// <summary>
		/// Make sure that our instance if null get his type and component
		/// </summary>
		/// <value>The instance.</value>
		public static T Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = FindObjectOfType<T> ();

					if (_instance == null)
					{
						GameObject obj = new GameObject ();

						_instance = obj.AddComponent<T> ();
					}
				}
				return _instance;
			}
		}
		
		/// <summary>
		/// On awake, we check if there's already a copy of the object in the scene. If there's one, we destroy it.
		/// </summary>
		protected virtual void Awake ()
		{
			
			
			if(_instance == null)
			{
				//If I am the first instance, make me the Singleton
				_instance = this as T;

				DontDestroyOnLoad (transform.gameObject);
			}
			else
			{
				//If a Singleton already exists and you find
				//another reference in scene, destroy it!
				if(this != _instance)
				{
					Destroy(this.gameObject);
				}
			}
		}

	}


}
